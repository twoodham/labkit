package errortracking

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_applyTrackerOptions(t *testing.T) {
	tests := []struct {
		name string
		opts []TrackerOption
		want trackerConfig
	}{
		{
			name: "with-all-options",
			opts: []TrackerOption{
				WithSentryDSN("dsn"),
				WithVersion("ver"),
				WithSentryEnvironment("env"),
				WithLoggerName("test-logger"),
			},
			want: trackerConfig{
				sentryDSN:         "dsn",
				version:           "ver",
				sentryEnvironment: "env",
				loggerName:        "test-logger",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := applyTrackerOptions(tt.opts)

			require.Equalf(t, got, tt.want, "applyTrackerOptions() = %v, want %v", got, tt.want)
		})
	}
}
