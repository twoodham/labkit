package errortracking

import (
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/assert"
)

func TestWithUserID(t *testing.T) {
	event := sentry.NewEvent()
	config := &captureConfig{}

	WithUserID("123")(config, event)

	assert.Equal(t, "123", event.User.ID)
}
