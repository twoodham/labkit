package sqlmetrics_test

import (
	"database/sql"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/labkit/metrics/sqlmetrics"
)

func ExampleNewDBStatsCollector() {
	// Open connection to database
	db, err := sql.Open("postgres", "postgres://postgres:postgres@localhost:5432/mydb")
	if err != nil {
		panic(err)
	}

	// Create a new collector
	collector := sqlmetrics.NewDBStatsCollector("mydb", db)

	// Register collector with Prometheus
	prometheus.MustRegister(collector)
}
