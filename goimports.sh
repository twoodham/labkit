#!/usr/bin/env sh

set -eux

find . -name \*.go  -exec goimports -w {} \;

