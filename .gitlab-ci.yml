image: golang:1.16

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  REPO_NAME: gitlab.com/gitlab-org/labkit
  GO_SEMANTIC_RELEASE_VERSION: 2.12.0

  # Note, GOLANGCI_LINT_VERSION should match .tool-versions!
  GOLANGCI_LINT_VERSION: 1.41.1

.go-version-matrix:
  parallel:
    matrix:
      - GO_VERSION: ["1.16", "1.17"]

backwards_compat:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.7-golang-1.16-git-2.31
  parallel:
    matrix:
      - REPO:
        - https://gitlab.com/gitlab-org/gitaly.git
        - https://gitlab.com/gitlab-org/container-registry.git
        - https://gitlab.com/gitlab-org/gitlab-shell.git
        - https://gitlab.com/gitlab-org/gitlab-pages.git
  stage: verify
  script:
    - ./backwords_compat.sh ${REPO}

stages:
  - build
  - verify
  - release

build:
  image: golang:${GO_VERSION}-alpine
  extends: .go-version-matrix
  stage: build
  script:
    - ./compile.sh

test:
  extends: .go-version-matrix
  image: golang:${GO_VERSION}
  stage: verify
  script:
    - ./test.sh

# The verify step should always use the same version of Go as devs are
# likely to be developing with to avoid issues with changes in these tools
# between go versions. Since these are simply linter warnings and not
# compiler issues, we only need a single version

sast:
  stage: verify

dependency_scanning:
  stage: verify

mod tidy:
  stage: verify
  script:
    - ./tidy.sh

shellcheck:
  image:
    name: koalaman/shellcheck-alpine:latest
  stage: verify
  script:
    - shellcheck -s sh -S style $(ls *.sh | grep -v downstream)

lint:
  image:
    name: golangci/golangci-lint:v${GOLANGCI_LINT_VERSION}-alpine
    entrypoint: ["/bin/sh", "-c"]
  # git must be installed for golangci-lint's --new-from-rev flag to work.
  before_script:
  - apk add --no-cache git jq
  stage: verify
  script:
    - ./lint.sh
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

commitlint:
  stage: verify
  image: node:14-alpine3.12
  before_script:
  - apk add --no-cache git
  - npm install
  script:
  - npx commitlint --from ${CI_MERGE_REQUEST_DIFF_BASE_SHA} --to HEAD --verbose
  rules:
  - if: $CI_MERGE_REQUEST_IID

release:
  image: node:14
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  script:
    - npm install
    - $(npm bin)/semantic-release
