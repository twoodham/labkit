#!/usr/bin/env sh

set -eux

# Check go tidy
git diff go.sum go.mod > /tmp/gomod-"${CI_JOB_ID}"-before
go mod tidy
git diff go.sum go.mod > /tmp/gomod-"${CI_JOB_ID}"-after
diff -U0 /tmp/gomod-"${CI_JOB_ID}"-before /tmp/gomod-"${CI_JOB_ID}"-after

