/*
Package log provides sensible logging defaults for Labkit.

Labkit uses Logrus for logging.

Applications that use Labkit should rely directly on Labkit.

This package provides some utility methods for working with Labkit, including:

1. Initialize Logrus in a consistent manner
1. Using GitLab's defaults with logrus
1. Passing correlation-ids between contexts and logrus

For backward compatibility reasons, the log timestamp format defaults to time.RFC3339, as done by Logrus. Additionally,
for millisecond precision timestamps, it is possible to opt in for using ISO8601. At present this can be done by setting
the `GITLAB_ISO8601_LOG_TIMESTAMP` environment variable to any value. ISO8601 will become the default and only format in
the next major release.

Please review the examples for more details of how to use this package.
*/
package log
